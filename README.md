Multi-format TETR.IO skin splicer library

- `new SkinSplicer(format, images)`: Creates a splicer from a format name (see `#stats()`) and an array of one or two `Image`s or `Canvas`es.
- `SkinSplicer#get(piece, connection)`: Returns a list of partial parameters for calls to [`drawImage`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage) to draw the specified block. Takes the form of `[[img, sx, sy, sw, sh]]` (array of arrays). `piece` is one of z, l, o, s, i, j, t, ghost, hold, gb, dgb or topout. `connections` is a bitstring of corner, top, right, bottom, left flags indicating open connections. Invalid sets will default to `0b00000`.
- `SkinSplicer#set(piece, connection, image)`: Sets the texture for a piece. Destructive (i.e. not useful) for jstris connected format.
